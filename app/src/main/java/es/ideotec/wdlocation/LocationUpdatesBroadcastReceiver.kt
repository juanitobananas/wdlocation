package es.ideotec.wdlocation

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.google.android.gms.location.LocationResult

private const val TAG = "LUBR"

class LocationUpdatesBroadcastReceiver : BroadcastReceiver() {
    companion object {
        const val ACTION_PROCESS_UPDATES =
            "es.ideotec.wdlocation.LocationUpdatesBroadcastReceiver.action.PROCESS_UPDATES"
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        Log.d(TAG, "onReceive() context:$context, intent:$intent")
        if (intent?.action == ACTION_PROCESS_UPDATES) {
            LocationResult.extractResult(intent)?.let { result ->
                if (result.locations.isNotEmpty()) {
                    Log.d(TAG, "Received location ${result.lastLocation}")
                    context?.openFileOutput("log.txt", Context.MODE_PRIVATE).use { output -> output?.write(result.lastLocation.toString().toByteArray()) }
                }
            }
        }
    }
}